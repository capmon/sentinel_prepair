#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging
from datetime import date
import os

from eodag.utils.logging import setup_logging
from eodag.api.core import EODataAccessGateway
from eodag.utils import ProgressCallback
import rpg.mgrs as mgrs
from datetime import datetime, timedelta


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="download sentinel 1 fiel from s2 tiles")

    parser.add_argument("-t", "--s2_tiles", nargs='+', dest="s2_tiles",
                        help="name of S2 tiles where we want to download S1 L1C data", required=True)
    parser.add_argument("-w", "--workspace", dest="workspace", help="eodag workspace")
    parser.add_argument("-o", "--out_s1_dir", dest="out_s1_dir",
                        help="directory where downloading S1 data", required=True)
    parser.add_argument("-b", "--begin_date", dest="begin_date",
                        help="begin date for S1 data 'yyyy-mm-dd'", required=True)
    parser.add_argument("-e", "--end_date", dest="end_date",
                        help="end date for S1 data", required=True)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)
    setup_logging(verbose=2)

    workspace = args.workspace
    conf_path = os.path.join(os.path.abspath(workspace), 'eodag_conf.yml')
    dag = EODataAccessGateway(user_conf_file_path=conf_path)
    dag.set_preferred_provider(u'theia')

    date_begin = args.begin_date
    date_end = args.end_date
    logging.info("({0}, {1})".format(date_begin, date_end))
    tiles = args.s2_tiles

    for tile in tiles:
        if tile[0] == "T":
            tileid = tile[1:]
        else:
            tileid= tile

        logging.info("get boundary box of tile {0}".format(tile))
        product_type_S2_L2A = 'S2_MSI_L2A'
        south_west = mgrs.MGRStoLL("31TFM2000020000")
        north_east = mgrs.MGRStoLL("31TFM8000080000")
        logging.info("approx south_west {0}".format(south_west))
        logging.info("approx north_east {0}".format(north_east))
        extent = {
            'lonmin': south_west["lon"],
            'lonmax': north_east["lon"],
            'latmin': south_west["lat"],
            'latmax': north_east["lat"],
        }
        s2_begin = datetime.today() - timedelta(days=15)
        products = dag.search(
            product_type_S2_L2A,
            startTimeFromAscendingNode=s2_begin.date().isoformat(),
            completionTimeFromAscendingNode=date.today().isoformat(),
            geometry=extent
        )
        s2_tile_geom = None
        for product in products:
            prop_tile_dict = product.as_dict()["properties"]
            name = prop_tile_dict["title"]
            logging.info("title \n {0}".format(name))
            # logging.info("keys \n {0}".format(prop_tile_dict.keys()))
            if tileid in name:
                s2_tile_geom = product.as_dict()["geometry"]
                break
        logging.info("s2_tile_geom \n {0}".format(s2_tile_geom))
        lon_list = [coord[0] for coord in s2_tile_geom["coordinates"][0]]
        lat_list = [coord[1] for coord in s2_tile_geom["coordinates"][0]]

        logging.info("search s1 tile {0}".format(tile))
        product_type_S1_GRD = 'S1_SAR_GRD'
        s1_search_extent = {
            'lonmin': min(lon_list),
            'lonmax': max(lon_list),
            'latmin': min(lat_list),
            'latmax': max(lat_list),
        }
        logging.info("s1_search_extent{0}".format(s1_search_extent))

        products_s1 = dag.search(
            product_type_S1_GRD,
            startTimeFromAscendingNode=date_begin,
            completionTimeFromAscendingNode=date_end,
            geometry=extent
        )
        orbits = []
        for s1_grd in products_s1:
            prop_s1_dict = s1_grd.as_dict()["properties"]
            name = prop_s1_dict["title"]
            logging.info("title \n {0}".format(name))
            logging.info("keys \n {0}".format(prop_s1_dict.keys()))
            s1_orbit = (prop_s1_dict["orbitNumber"], prop_s1_dict["orbitDirection"])
            if s1_orbit not in orbits:
                orbits.append(s1_orbit)
        logging.info("orbits {0}".format(orbits))






