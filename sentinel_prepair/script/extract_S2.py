#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging
import zipfile

import os
from tqdm import tqdm
from os import listdir, remove, makedirs
from os.path import isfile, join, exists
import xml.etree.ElementTree as ET


def list_theia_archive(in_dir):
    """
    renvoie une liste des archives S2 theia présentes dans un dossier

    la liste renvoie une liste de chemin de fichiers zip. On considere qu'un fichier
    zip est une archive THEIA quand son nom commence par "SENTINEL"

    :param in_dir: dossier contenant des archives sentinel 2 THEIA
    :return: liste de chemins de fichier zip de type archive theia
    """
    dir_files = [f for f in listdir(in_dir) if isfile(join(in_dir, f))]
    zip_files = [f for f in dir_files if f[-4:] == ".zip"]
    theia_zip = [os.path.abspath(os.path.join(in_dir,f)) for f in zip_files if f[:8] == "SENTINEL"]

    return theia_zip


def rm_theia_sre(theia_archive_dir):
    """
    supprime les fichiers SRE d'unea archive THEIA

    :param theia_archive_dir:
    :return:
    """
    all_theia_files = listdir(theia_archive_dir)
    # logging.info("all_theia_files: {0}".format(all_theia_files))

    all_theia_sre = [f for f in all_theia_files if f.find("_SRE_") != -1]
    # logging.info("find sre files : {0}".format(all_theia_sre))

    for sre in all_theia_sre:
        abs_sre = os.path.abspath(os.path.join(theia_archive_dir, sre))
        logging.info("remove {0}".format(abs_sre))
        remove(abs_sre)


def get_rel_orbit(abs_orbit, sat_id):
    if sat_id == "SENTINEL2B":
        return ((abs_orbit - 27) % 143) + 1
    elif sat_id == "SENTINEL2A":
        return ((abs_orbit + 2) % 143) + 1
    else:
        return None


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="extract s2 tiles and delete unused file")

    parser.add_argument(
        "-i", "--in_s2_dir", dest="in_s2_dir", help="input dir containing s2 tile form theia", required=True)
    parser.add_argument(
        "-o", "--out_dir", dest="out_s2_dir", help="input dir containing s2 tile form theia", required=True)
    parser.add_argument(
        "-s", "--rm_sre", dest="rm_sre", type=str2bool, help="delete sre file in extract dir", required=False)
    parser.add_argument(
        "-d", "--rm_archive", dest="rm_archive", type=str2bool, help="delete input theia archive after decompression",
        required=False)
    parser.add_argument(
        "-r", "--rel_orbit", dest="rel_orbit", type=int, help="filter by relative orbit number before extract",
        required=False)
    parser.add_argument(
        "-m", "--maxcloud", dest="maxcloud", type=int, help="filter by maxcloud cover before extract",
        required=False)

    args = parser.parse_args()

    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    out_dir = args.out_s2_dir
    if not exists(out_dir):
        makedirs(out_dir)

    out_dir = os.path.abspath(out_dir)
    if args.rm_sre is not None:
        rm_sre = args.rm_sre
    else:
        rm_sre = True
    if args.rm_archive is not None:
        rm_archive = args.rm_archive
    else:
        rm_archive = False
    logging.info("rm_archive = {0} ".format(rm_archive))

    all_theia_archives = list_theia_archive(args.in_s2_dir)
    logging.info("find  {0} archives".format(len(all_theia_archives)))

    # pbar = tqdm(total=len(all_theia_archives))
    for theia_archive in all_theia_archives:
        logging.info("decompress archive : {0} to {1}".format(theia_archive, out_dir))

        with open(theia_archive, 'rb') as theia_zip:
            zf = zipfile.ZipFile(theia_zip)
            dirs = list(set([os.path.dirname(x) for x in zf.namelist()]))
            dirs.sort(key=len)
            logging.info("dirs : {0}".format(dirs))
            topdir = dirs[0]
            extract_dir = os.path.join(out_dir, topdir)
            logging.info("extract_dir : {0}".format(extract_dir))

            # print(zf.namelist())
            metadata = [name for name in zf.namelist() if ".xml" in name]
            print(metadata)
            cloud_cover = None
            rel_orbit = None
            if len(metadata) != 0:
                meta_xml = metadata[0]
                with zf.open(meta_xml) as zmeta:
                    tree = ET.parse(zmeta)
                    root = tree.getroot()
                    # namespace = "{http://www.w3.org/2001/XMLSchema-instance}"

                    xpath_prodqual = "Quality_Informations/Current_Product/Product_Quality_List/Product_Quality/"
                    xpath_cloudpercent = xpath_prodqual + "Global_Index_List/QUALITY_INDEX[@name='CloudPercent']"
                    print("search cloud")
                    for meta_cloud in tree.findall(xpath_cloudpercent):
                        cloud_cover = int(meta_cloud.text)
                        logging.info("cloud cover: {0}".format(cloud_cover))
                    logging.info("cloud_cover: {0}".format(cloud_cover))

                    xpath_orbit = "Product_Characteristics/ORBIT_NUMBER"
                    orbit = None
                    print("search orbit")
                    for meta_orbit in tree.findall(xpath_orbit):
                        orbit = int(meta_orbit.text)
                    logging.info("orbit: {0}".format(orbit))

                    sat = None
                    xpath_sat = "Product_Characteristics/PLATFORM"
                    print("search plateform")
                    for meta_sat in tree.findall(xpath_sat):
                        sat = meta_sat.text
                    logging.info("sat: {0}".format(sat))

                    rel_orbit = get_rel_orbit(orbit, sat)
                    logging.info("rel_orbit: {0}".format(rel_orbit))

            if args.rel_orbit is not None and rel_orbit is not None:
                if int(args.rel_orbit) != rel_orbit:
                    continue

            if args.maxcloud is not None and cloud_cover is not None:
                if int(args.maxcloud) < cloud_cover:
                    continue

            if not exists(extract_dir):
                logging.info("extract ")
                zf.extractall(path=out_dir)

        if rm_sre:
            rm_theia_sre(extract_dir)

        if rm_archive:
            logging.info("remove archive : {0}".format(theia_archive))
            remove(theia_archive)

        # pbar.update(1)

    # pbar.close()
