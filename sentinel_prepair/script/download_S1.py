#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging
import re
import collections
import os
from time import sleep
from datetime import datetime, timedelta
import shutil
import hashlib

from sentinelsat import SentinelAPI, InvalidChecksumError
from sentinelsat.sentinel import _check_scihub_response
import shapely.wkt
from shapely.geometry import shape
from contextlib import closing
from tqdm import tqdm
import requests
from six.moves.urllib.parse import urljoin


import urllib3
urllib3.disable_warnings()

# TO MODIFY
api = SentinelAPI('scihub_login', 'scihub_psw', 'https://scihub.copernicus.eu/dhus')
peps_auth = ("pesp_login", 'peps_psw')
APIKEY = "SOBLOO_APIKEY"


def md5_compare(file_path, checksum, block_size=2 ** 13):
    """Compare a given MD5 checksum with one calculated from a file."""
    with closing(tqdm(desc="MD5 checksumming", total=os.path.getsize(file_path), unit="B",
                            unit_scale=True)) as progress:
        md5 = hashlib.md5()
        with open(file_path, "rb") as f:
            while True:
                block_data = f.read(block_size)
                if not block_data:
                    break
                md5.update(block_data)
                progress.update(len(block_data))
        return md5.hexdigest().lower() == checksum.lower()


def get_resto_id(identifier, peps_session):

    url = urljoin("https://peps.cnes.fr/", "resto/api/collections/S1/search.json?identifier={0}".format(identifier))
    print(url)

    response = peps_session.get(url, auth=peps_session.auth)
    _check_scihub_response(response)

    resp_dict = response.json()

    features_dict = resp_dict['features']
    if len(features_dict) == 0:
        return None

    product = resp_dict['features'][0]

    output = {
        'id': product['id'],
        'size': int(product['properties']['resourceSize']),
        'md5': product['properties']['resourceChecksum']
    }
    return output


# https://peps.cnes.fr/resto/api/collections/S1/search.json?identifier=S1A_IW_GRDH_1SDV_20170121T062355_20170121T062420_014926_0185BB_6091
def peps_download(product_name, directory_path, peps_session, checksum=True):
    """

    :param product_name:
    :param directory_path:
    :param peps_session:
    :param checksum:
    :return:
    """
    product_info = get_resto_id(product_name, peps_session)
    if product_info is None:
        logging.warning("product {0} is not present in resto S1 archive".format(product_name))
        return

    path = os.path.join(directory_path, product_name + '.zip')
    product_info['path'] = path
    product_info['downloaded_bytes'] = 0

    logging.info('Downloading %s to %s', id, path)

    if os.path.exists(path):
        # We assume that the product has been downloaded and is complete
        return product_info

    # Use a temporary file for downloading
    temp_path = path + '.incomplete'

    skip_download = False
    if os.path.exists(temp_path):
        if os.path.getsize(temp_path) > product_info['size']:
            logging.warning(
                "Existing incomplete file %s is larger than the expected final size"
                " (%s vs %s bytes). Deleting it.",
                str(temp_path), os.path.getsize(temp_path), product_info['size'])
            os.remove(temp_path)
        elif os.path.getsize(temp_path) == product_info['size']:
            if md5_compare(temp_path, product_info['md5']):
                skip_download = True
            else:
                # Log a warning since this should never happen
                logging.warning(
                    "Existing incomplete file %s appears to be fully downloaded but "
                    "its checksum is incorrect. Deleting it.",
                    str(temp_path))
                os.remove(temp_path)
        else:
            # continue downloading
            logging.info(
                "Download will resume from existing incomplete file %s.", temp_path)
            pass

    if not skip_download:
        # Store the number of downloaded bytes for unit tests
        peps_url = "https://peps.cnes.fr/resto/collections/S1/{0}/download/?issuerId=peps".format(product_info['id'])
        print("peps_url {0}".format(peps_url))
        product_info['downloaded_bytes'] = url_download(
            peps_url, temp_path, peps_session, product_info['size'])

    # Check integrity with MD5 checksum
    logging.warning("resto md5 checksum {0}".format(product_info['md5']))
    if checksum is True and product_info['md5']:
        if not md5_compare(temp_path, product_info['md5']):
            os.remove(temp_path)
            raise InvalidChecksumError('File corrupt: checksums do not match')

    # Download successful, rename the temporary file to its proper name
    shutil.move(temp_path, path)
    return product_info


def sobloo_download(prod_id, directory_path, session, prod_name=None):
    """

    :param uid:
    :param directory_path:
    :param session
    :param product_name:

    :return:
    """

    if prod_name is not None:
        cmd = "https://sobloo.eu/api/v1/services/search?f=identification.externalId:eq:" + prod_name
        # "&include=previews,identification&pretty=true"
        # this API does not need authentication
    else:
        cmd = "https://sobloo.eu/api/v1/services/search?f=data.uid:eq:" + prod_id
        # "&include=previews,identification&pretty=true"

    r = session.get(cmd, verify=False)
    if r.status_code != requests.codes.ok:
        print("Error: %s  for the request cmd <%s>." % (r.status_code, cmd))
        return -1
    else:
        # the returned json object contains the internal Id (only one product returned)
        # print(r.json())
        j = r.json()
        if j["totalnb"] == 0:
            print("No matching products found")
            return -1
        else:
            product = j['hits'][0]

    product_name = product["data"]["identification"]["externalId"]
    uid = product["data"]["uid"]

    path = os.path.join(directory_path, product_name + '.zip')
    # print(product)
    product['path'] = path
    product['downloaded_bytes'] = 0

    logging.info('Downloading %s to %s', product_name, path)

    if os.path.exists(path):
        # We assume that the product has been downloaded and is complete
        return product

    # Use a temporary file for downloading
    temp_path = path + '.incomplete'

    skip_download = False
    # # no information/metadata of archive compress size, only decompress size.
    # if os.path.exists(temp_path):
    #     file_size_mb = os.path.getsize(temp_path) >> 20
    #     if file_size_mb > product["data"]['archive']['size']:
    #         logging.warning(
    #             "Existing incomplete file %s is larger than the expected final size"
    #             " (%s vs %s bytes). Deleting it.",
    #             str(temp_path), file_size_mb, product['archive']['size'])
    #         os.remove(temp_path)
    #     elif os.path.getsize(temp_path) == product['archive']['size']:
    #         skip_download = True
    #         logging.info("Existing file %s appears to be fully downloaded, skip download", str(temp_path))
    #     else:
    #         # continue downloading
    #         logging.info("Download will resume from existing incomplete file %s.", temp_path)
    #         pass

    if not skip_download:
        # Store the number of downloaded bytes for unit tests
        sobloo_url = "https://free.sobloo.eu/api/v1/services/download/" + product["data"]["uid"]
        # we inject here the APIKEY secret got from our account page
        headers = {"Authorization": "Apikey " + APIKEY}
        print("sobloo_url {0}".format(sobloo_url))

        downloaded_bytes = 0
        with closing(session.get(sobloo_url, stream=True, auth=session.auth, headers=headers)) as r, \
                closing(tqdm(desc="Downloading", unit="B", unit_scale=True)) as progress:

            _check_scihub_response(r, test_json=False)
            chunk_size = 2 ** 20  # download in 1 MB chunks
            mode = 'wb'
            with open(temp_path, mode) as f:
                for chunk in r.iter_content(chunk_size=chunk_size):
                    if chunk:  # filter out keep-alive new chunks
                        f.write(chunk)
                        progress.update(len(chunk))
                        downloaded_bytes += len(chunk)

        # product['downloaded_bytes'] = url_download(
        #    sobloo_url, temp_path, session, product['data']['archive']['size'] << 20, headers)

    # Download successful, rename the temporary file to its proper name
    shutil.move(temp_path, path)
    return product


def url_download(peps_url, path, session, file_size, headers=None):

    if headers is None:
        headers = {}

    continuing = os.path.exists(path)
    if continuing:
        already_downloaded_bytes = os.path.getsize(path)
        headers = {'Range': 'bytes={}-'.format(already_downloaded_bytes)}
    else:
        already_downloaded_bytes = 0
    downloaded_bytes = 0
    with closing(session.get(peps_url, stream=True, auth=session.auth, headers=headers)) as r, \
            closing(tqdm(desc="Downloading", total=file_size, unit="B", unit_scale=True,
                         initial=already_downloaded_bytes)) as progress:
        _check_scihub_response(r, test_json=False)
        chunk_size = 2 ** 20  # download in 1 MB chunks
        mode = 'ab' if continuing else 'wb'
        with open(path, mode) as f:
            for chunk in r.iter_content(chunk_size=chunk_size):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    progress.update(len(chunk))
                    downloaded_bytes += len(chunk)
        # Return the number of bytes downloaded
        return downloaded_bytes


def get_tile_geom_from_scihub(tile_id):
    """

    :param tile_id:
    :return:
    """
    query_kwargs = {
        'platformname': 'Sentinel-2',
        'producttype': 'S2MSI1C',
        'date': ('NOW-21DAYS', 'NOW'),
        'tileid': tile_id
    }
    pp = api.query(**query_kwargs)
    ids = list(pp)
    footprint_wkt = pp[ids[0]]["footprint"]
    s2_tile_geo = shapely.wkt.loads(footprint_wkt)
    for res_id in ids[1:]:
        footprint_wkt = pp[res_id]["footprint"]
        id_geom = shapely.wkt.loads(footprint_wkt)
        if id_geom.area > s2_tile_geo.area:
            s2_tile_geo = id_geom

    logging.info("footprint {0}".format(pp[ids[0]]["footprint"]))
    bb_wkt = s2_tile_geo.wkt
    # query some s1 tiles for getting footprint by relative orbit
    bb_wkt = re.sub(r'(?<!\d) ', '', bb_wkt)

    return s2_tile_geo, bb_wkt


def search_s1_product_from_scihub(wkt_bbox, begin_date, end_date, s1_type="GRD"):
    """

    :param wkt_bbox:
    :param begin_date:
    :param end_date:
    :return:
    """
    logging.info("bbox_wkt {0}".format(bbox_wkt))
    if s1_type in ("GRD", "SLC"):
        query_kwargs = {
            'platformname': 'Sentinel-1',
            'polarisationmode': 'VV VH',
            'producttype': s1_type,
            'sensoroperationalmode': "IW"
        }
    else:
        raise ValueError ("s1 type must be GRD or SLC and not {0}".format(s1_type))

    # "'footprint': bbox_wkt
    s1_products = api.query(area=wkt_bbox, date=(begin_date, end_date), **query_kwargs)

    return s1_products


def search_s1_product_from_sobloo(wkt_bbox, begin_date, end_date, s1_type="GRD"):
    """

    :param wkt_bbox:
    :param begin_date:
    :param end_date:
    :return:
    """

    search_url = "https://sobloo.eu/api/v1/services/"

    query = ""
    query += "&f=acquisition.mission:eq:Sentinel-1"
    query += "&f=identification.type:eq:GRD"
    begin_datetime= datetime.strptime(begin_date, '%Y%m%d')
    begin_timestamp = int(1e3 * begin_datetime.timestamp())
    end_datetime= datetime.strptime(end_date, '%Y%m%d')
    end_timestamp = int(1e3 * end_datetime.timestamp())
    query += "&f=acquisition.beginViewingDate:gte:{0}".format(begin_timestamp)
    query += "&f=acquisition.endViewingDate:lte:{0}".format(end_timestamp)
    query += "&gintersect={0}".format(wkt_bbox)
    print(query)

    products, count = sobloo_load_query(query, page_size=50, search_url=search_url)

    print("find {0} sobloo products".format(count))

    return products


def sobloo_load_query(
        query, order_by=None, limit=None, offset=0, page_size=100, search_url="https://sobloo.eu/api/v1/services/"):

    products, count = sobloo_load_subquery(query, order_by, limit, offset, page_size, search_url)

    # repeat query until all results have been loaded
    max_offset = count
    if limit is not None:
        max_offset = min(count, offset + limit)
    if max_offset > offset + page_size:
        progress = tqdm(desc="Querying products", initial=page_size, total=max_offset - offset, unit=' products')
        for new_offset in range(offset + page_size, max_offset, page_size):
            new_limit = limit
            if limit is not None:
                new_limit = limit - new_offset + offset
            ret = sobloo_load_subquery(query, order_by, new_limit, new_offset, page_size, search_url)[0]
            progress.update(len(ret))
            products += ret
        progress.close()

    return products, count


def sobloo_load_subquery(
        query, order_by=None, limit=None, offset=0, page_size=50, search_url="https://sobloo.eu/api/v1/services/"):
    """

    :param query:
    :param order_by:
    :param limit:
    :param offset:
    :return:
    """
    # load query results
    if limit is None:
        limit = page_size
    limit = min(limit, page_size)
    url = 'search?format=json&&size={}'.format(limit)
    url += '&&from={}'.format(offset)
    if order_by:
        url += '&orderby={}'.format(order_by)
    url = urljoin(search_url, url)
    # print(url)

    session = requests.Session()
    response = session.get(url+query, verify=False)
    # print(response.url)
    session.close()

    # print(response)
    # parse response content
    try:
        json_res = response.json()
        if json_res['totalnb'] is None:
            # We are using some unintended behavior of the server that a null is
            # returned as the total results value when the query string was incorrect.
            raise ValueError('Invalid query string. Check the parameters and format.', response)
        total_results = int(json_res['totalnb'])
    except (ValueError, KeyError):
            raise ValueError('API response not valid. JSON decoding failed.', response)

    products = json_res.get('hits', [])
    # this verification is necessary because if the query returns only
    # one product, self.products will be a dict not a list
    if isinstance(products, dict):
        products = [products]

    return products, total_results


def get_s1_slice_orbit_intersect_from_product(s1_products, test_geom):
    """
    renvoie un dictionnaire contenant le % d'intersection entre les emprises s1 et  test_geom

    les emprise s1 sont regroupées par orbite_relative/id_satellite/slice similaires

    :param s1_products:
    :param test_geom:
    :return:
    """

    slice_orbit_dict = {}
    for s1_product in s1_products.items():

        s1_data_dict = s1_product[1]
        s1_rel_orbit = s1_data_dict["relativeorbitnumber"]
        if s1_rel_orbit not in slice_orbit_dict:
            slice_orbit_dict[s1_rel_orbit] = dict()

        rel_orbit_dict = slice_orbit_dict[s1_rel_orbit]
        s1_sat_id = s1_data_dict["filename"][0:3]
        if s1_sat_id not in rel_orbit_dict:
            rel_orbit_dict[s1_sat_id] = dict()

        sat_dict = rel_orbit_dict[s1_sat_id]
        slice = s1_data_dict["slicenumber"]
        if slice not in sat_dict:
            s1_wkt = s1_data_dict["footprint"]
            s1_geom = shapely.wkt.loads(s1_wkt)
            intersect_geom = s1_geom.intersection(test_geom)
            intersect_area = intersect_geom.area / test_geom.area
            sat_dict[slice] = intersect_area

    return slice_orbit_dict


def get_max_intersect_s1_orbit_scihub(s1_products, s2_tile_geom, sum_area_threshold):
    """
    renvoie les orbites relative s1 dont l'union des différentes slcies à un % d'intersection > sum_area_threshold

    sum_area_threshold entre 0 et 1

    :param s1_products:
    :param s2_tile_geom:
    :param sum_area_threshold:
    :return:
    """
    slice_orbit_dict = get_s1_slice_orbit_intersect_from_product(s1_products, s2_tile_geom)

    logging.info("intersect_dict\n{0}".format(slice_orbit_dict))
    keep_rel_orbit = []
    for orbit in slice_orbit_dict.items():
        for s1_sat in orbit[1].items():
            sum_area_p = 0
            for s1_slice in s1_sat[1].items():
                sum_area_p += s1_slice[1]
            logging.info("rel_orbit {0}, sat {1},  area {2}".format(orbit[0], s1_sat[0], sum_area_p))
            if sum_area_p > sum_area_threshold:
                keep_rel_orbit.append((orbit[0], s1_sat[0]))

    return keep_rel_orbit


def get_max_intersect_s1_orbit_sobloo(s1_products, s2_tile_geom, sum_area_threshold):
    """

    :param s1_products:
    :param s2_tile_geom:
    :param sum_area_threshold:

    :return:
    """
    orbit_dict = {}
    for s1_product in s1_products:

        s1_data_dict = s1_product["data"]

        s1_rel_orbit = s1_data_dict["orbit"]["relativeNumber"]
        if s1_rel_orbit not in orbit_dict:
            orbit_dict[s1_rel_orbit] = dict()

        rel_orbit_dict = orbit_dict[s1_rel_orbit]
        s1_sat_id = s1_data_dict["identification"]["externalId"][0:3]
        if s1_sat_id not in rel_orbit_dict:
            s1_geojson = s1_product["md"]["geometry"]
            s1_geom = shape(s1_geojson)
            rel_orbit_dict[s1_sat_id] = s1_geom
        else:
            # print("update geom")
            s1_geojson = s1_product["md"]["geometry"]
            s1_geom = shape(s1_geojson)
            # print("{0} {1} \n {2}".format(s1_rel_orbit, s1_sat_id, s1_geojson))
            old_geom = rel_orbit_dict[s1_sat_id]
            # print("area before {0}".format(old_geom.area))
            rel_orbit_dict[s1_sat_id] = old_geom.union(s1_geom)
            # print("area after {0}".format(rel_orbit_dict[s1_sat_id].area))

    # intersect_geom = s1_geom.intersection(s2_tile_geom)
    # intersect_area = intersect_geom.area / s2_tile_geom.area
    # sat_dict[slice] = intersect_area
    # print(orbit_dict)
    keep_rel_orbit = []
    # print(s2_tile_geom.wkt)
    for rel_orbit, sat_id_dict in orbit_dict.items():
        for s1_sat_id, geom in sat_id_dict.items():
            # print(geom.wkt)
            intersect_geom = geom.intersection(s2_tile_geom)
            # print(intersect_geom.wkt)
            intersect_ratio = intersect_geom.area / s2_tile_geom.area
            print("intersect_ratio rel_orbit {0} sat {1} = {2}".format(rel_orbit, s1_sat_id, intersect_ratio))
            if intersect_ratio > sum_area_threshold:
                keep_rel_orbit.append((rel_orbit, s1_sat_id))

    return keep_rel_orbit


def filter_s1_product_by_orbit_scihub(s1_products, keep_rel_orbit):
    """

    :param s1_products:
    :param keep_rel_orbit:
    :return:
    """
    remove_product = []
    for s1_product in s1_products.items():
        data_dict = s1_product[1]
        rel_orbit = data_dict["relativeorbitnumber"]
        id_sat = data_dict["filename"][0:3]
        if not (rel_orbit, id_sat) in keep_rel_orbit:
            remove_product.append(s1_product[0])

    for s1_id in remove_product:
        s1_products.pop(s1_id)

    return s1_products


def filter_s1_product_by_orbit_sobloo(s1_products, keep_rel_orbit):
    """

    :param s1_products:
    :param keep_rel_orbit:
    :return:
    """
    keep_product = []
    remove_product = []
    for s1_product in s1_products:

        s1_data_dict = s1_product["data"]

        s1_rel_orbit = s1_data_dict["orbit"]["relativeNumber"]
        s1_sat_id = s1_data_dict["identification"]["externalId"][0:3]
        # s1_id = s1_data_dict["identification"]["externalId"]
        if not (s1_rel_orbit, s1_sat_id) in keep_rel_orbit:
            remove_product.append(s1_product)
        else:
            keep_product.append(s1_product)

    return keep_product


def download_peps_s1_products(s1_products):
    """

    :param s1_products:
    :return:
    """
    peps_session = requests.Session()
    if peps_auth:
        peps_session.auth = peps_auth
    product_ids = list(s1_products)
    logging.info("Will download %d products", len(s1_products))
    for i, product_id in enumerate(s1_products):
        for attempt_num in range(2):
            try:
                product_info = api.get_product_odata(product_id)
                product_name = product_info['title']
                peps_product_info = peps_download(product_name, out_path, peps_session, checksum=True)
            except (KeyboardInterrupt, SystemExit):
                raise
            except InvalidChecksumError as e:
                logging.warning(
                    "Invalid checksum. The downloaded file for '%s' is corrupted.", product_id)
            except Exception as e:
                logging.exception("There was an error downloading %s", product_id)
        logging.info("%s/%s products downloaded", i + 1, len(product_ids))


def download_s1_products_sobloo(s1_products):
    """

    :param s1_products:
    :return:
    """
    sobloo_session = requests.Session()

    logging.info("Will download %d products", len(s1_products))
    nb_download = 0
    for uid, product_s1 in s1_products.items():
        s1_data_dict = product_s1["data"]
        product_uid = s1_data_dict["uid"]
        product_name = s1_data_dict["identification"]["externalId"]

        for attempt_num in range(2):
            try:
                product_info = sobloo_download(
                    product_uid, out_path, sobloo_session, prod_name=product_name)
            except (KeyboardInterrupt, SystemExit):
                raise
            except InvalidChecksumError as e:
                logging.warning(
                    "Invalid checksum. The downloaded file for '%s' is corrupted.", product_name)
            except Exception as e:
                logging.exception("There was an error downloading %s", product_name)
        nb_download += 1
        logging.info("%s/%s products downloaded", nb_download, len(s1_products))


def scihub_split_online_offline_product(products):
    """

    :param products:
    :return:
    """
    products_online = dict()
    products_offline = dict()
    for product in products.items():
        product_info = api.get_product_odata(product[0])
        logging.info("product_info {0}".format(product_info))
        url = product_info['url']
        online = product_info['online']
        logging.info("online {0}".format(online))

        if not online:
            products_offline[product[0]] = product[1]
        else:
            products_online[product[0]] = product[1]

        logging.info("download url {0}".format(url))

    return products_online, products_offline


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="download sentinel 1 file from s2 tiles")

    parser.add_argument("-t", "--s2_tiles", nargs='+', dest="s2_tiles",
                        help="name of S2 tiles where we want to download S1 L1C data", required=True)
    parser.add_argument("-s", "--s2_tiles_shp", dest="s2_tiles_shp", help="shapefile with S2 tiles boundary in WGS84")
    parser.add_argument("-o", "--out_s1_dir", dest="out_s1_dir",
                        help="directory where downloading S1 data", required=True)
    parser.add_argument("-b", "--begin_date", dest="begin_date",
                        help="begin date for S1 data 'yyyy-mm-dd'", required=True)
    parser.add_argument("-e", "--end_date", dest="end_date",
                        help="end date for S1 data", required=True)
    parser.add_argument("-p", "--provider", dest="provider", default="peps",
                        help="data provider to choose in [scihub, sobloo, scihub_peps]", required=False)
    parser.add_argument("-s1", "--s1_type", dest="s1_type", default="GRD",
                        help="type of S1 data tpe to download [GRD, SLC]", required=False)

    args = parser.parse_args()
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)
    date_begin = args.begin_date
    date_end = args.end_date
    logging.info("({0}, {1})".format(date_begin, date_end))

    provider = args.provider
    print(provider)

    tiles = args.s2_tiles
    product_all = collections.OrderedDict()
    # search s1 products intersecting each s2 tiles
    for tile in tiles:
        if tile[0] == "T":
            tileid = tile[1:]
        else:
            tileid = tile

        logging.info("get boundary box of tile {0}".format(tile))
        s2_tile_geom, bbox_wkt = get_tile_geom_from_scihub(tileid)
        logging.info("bbox_wkt {0}".format(bbox_wkt))

        # search all s1 products intersecting s2 tiles
        if provider in ["scihub", "scihub_peps"]:
            products_s1 = search_s1_product_from_scihub(bbox_wkt, date_begin, date_end, args.s1_type)
        elif provider == "sobloo":
            products_s1 = search_s1_product_from_sobloo(bbox_wkt, date_begin, date_end, args.s1_type)
            print(len(products_s1))
        else:
            raise ValueError("provider {0} is not yet supported".format(provider))

        # filter s1 products. keep only s1 products belonging to orbit with a great intersection with s2_tile_geom
        if provider in ["scihub", "scihub_peps"]:
            keep_orbit = get_max_intersect_s1_orbit_scihub(products_s1, s2_tile_geom, 0.2)
            logging.info("keep_orbit {0}".format(keep_orbit))
            products_s1 = filter_s1_product_by_orbit_scihub(products_s1, keep_orbit)
        elif provider == "sobloo":
            keep_orbit = get_max_intersect_s1_orbit_sobloo(products_s1, s2_tile_geom, 0.2)
            logging.info("keep_orbit {0}".format(keep_orbit))
            logging.info("nb product before filter  {0}".format(len(products_s1)))
            products_s1 = filter_s1_product_by_orbit_sobloo(products_s1, keep_orbit)
            logging.info("nb product after filter  {0}".format(len(products_s1)))
        else:
            print("no filtering")

        # update all product list
        if provider == "sobloo":
            products_s1_dict = {product["data"]["uid"]: product for product in products_s1}
            product_all.update(products_s1_dict)
        else:
            product_all.update(products_s1)

    logging.info("nb product {0}".format(len(product_all)))

    out_path = os.path.abspath(args.out_s1_dir)
    logging.info("out_path {0}".format(out_path))

    if provider in ["scihub", "scihub_peps"]:
        product_all_online, product_all_offline = scihub_split_online_offline_product(product_all)
        product_all_online_ids = list(product_all_online.keys())
        product_all_offline_ids = list(product_all_offline.keys())
        logging.info("nb online product {0}".format(len(product_all_online)))
        logging.info("nb offline product {0}".format(len(product_all_offline)))
        begin_time = datetime.now()

        if provider == "scihub":
            # try to activate offlines products.
            if len(product_all_offline) > 0:
                logging.info("activate offline")
                api.download_all(product_all_offline_ids, directory_path=out_path, max_attempts=1)

        # download of onlines products
        if len(product_all_online) > 0:
            logging.info("download online")
            api.download_all(product_all_online_ids, directory_path=out_path)

        # download of offline products
        if provider == "scihub":
            if len(product_all_offline) > 0:
                # on réessaye les offlines pendant 24h
                logging.info("waiting before retry offline")
                for i in range(24 * 6):
                    sleep(600)
                    curr_time = datetime.now()
                    logging.info("curr_time {0}".format(curr_time))
                    if curr_time > (begin_time + timedelta(hours=24)):
                        api.download_all(product_all_offline_ids, directory_path=out_path)
                        break
        else:
            # download with peps
            if len(product_all_offline) > 0:
                download_peps_s1_products(product_all_offline)
    else:
        print(next(iter(product_all.values())))
        download_s1_products_sobloo(product_all)
