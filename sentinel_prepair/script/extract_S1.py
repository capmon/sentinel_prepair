#!/usr/bin/env python
# coding: utf-8

#
# Copyright (c) 2020 IGN France.
#
# This file is part of lpis_prepair
# (see https://gitlab.com/capmon/lpis_prepair).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import logging
import zipfile

import os
from tqdm import tqdm
from os import listdir, remove, makedirs
from os.path import isfile, join, exists


def list_s1_archive(in_dir):
    """
    renvoie une liste des archives S2 theia présentes dans un dossier

    la liste renvoie une liste de chemin de fichiers zip. On considere qu'un fichier
    zip est une archive THEIA quand son nom commence par "SENTINEL"

    :param in_dir: dossier contenant des archives sentinel 2 THEIA
    :return: liste de chemins de fichier zip de type archive theia
    """
    dir_files = [f for f in listdir(in_dir) if isfile(join(in_dir, f))]
    zip_files = [f for f in dir_files if f[-4:] == ".zip"]
    s1_zip = [os.path.abspath(os.path.join(in_dir, f)) for f in zip_files if f[:2] == "S1"]

    return s1_zip


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="extract s2 tiles and delete unused file")

    parser.add_argument(
        "-i", "--in_s1_dir", dest="in_s1_dir", help="input dir containing s1 safe compress", required=True)
    parser.add_argument(
        "-o", "--out_dir", dest="out_s1_dir", help="output dir for s1 safe uncompress ", required=True)
    parser.add_argument(
        "-d", "--rm_archive", dest="rm_archive", type=str2bool, help="delete input archive after decompression",
        required=False)
    parser.add_argument(
        "-n", "--num_extract", dest="num_extract", type=int, help="number of archive to extract", required=False)

    args = parser.parse_args()

    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    out_dir = args.out_s1_dir
    if not exists(out_dir):
        makedirs(out_dir)

    out_dir = os.path.abspath(out_dir)

    if args.rm_archive is not None:
        rm_archive = args.rm_archive
    else:
        rm_archive = False

    logging.info("rm_archive  {0} ".format(rm_archive))

    if args.num_extract is not None:
        num_extract = args.num_extract
    else:
        num_extract = -1

    all_s1_archives = list_s1_archive(args.in_s1_dir)
    logging.info("find  {0} archives".format(len(all_s1_archives)))

    # pbar = tqdm(total=len(all_theia_archives))

    for s1_archive in all_s1_archives[:num_extract]:
        logging.info("decompress archive : {0} to {1}".format(s1_archive, out_dir))

        with open(s1_archive, 'rb') as peps_zip:
            zf = zipfile.ZipFile(peps_zip)
            dirs = list(set([os.path.dirname(x) for x in zf.namelist()]))
            dirs.sort(key=len)
            logging.info("dirs : {0}".format(dirs))
            topdir = dirs[0]
            extract_dir = os.path.join(out_dir, topdir)
            logging.info("extract_dir : {0}".format(extract_dir))

            if not exists(extract_dir):
                logging.info("extract ")
                zf.extractall(path=out_dir)

        if rm_archive:
            logging.info("remove archive : {0}".format(s1_archive))
            remove(s1_archive)

        # pbar.update(1)

    # pbar.close()
